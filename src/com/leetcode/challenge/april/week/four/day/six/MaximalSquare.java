package com.leetcode.challenge.april.week.four.day.six;

public class MaximalSquare {

    public static void main(String[] args) {
        char[][] matrix = new char[][]{
                {'0', '1','1'},
                {'0', '1','1'},
                {'0', '1','0'}
        };

        MaximalSquare maximalSquare = new MaximalSquare();
        System.out.println(maximalSquare.maximalSquare(matrix));
    }

    public int maximalSquare(char[][] matrix) {
        int max = 0;
        for (int j = 0; j < matrix.length; j++) {
            for (int i = 0; i < matrix[j].length; i++) {
                if (matrix[j][i] == '1') {
                    max = Math.max(max, findSquare(matrix, j + 1, i + 1, 1));
                }
            }
        }

        return max * max;
    }

    public int findSquare(char matrix[][], int j, int i, int size) {

        if (j == matrix.length || i == matrix[j].length || matrix[j][i] != '1') {
            return size;
        }

        int y = j;
        int x = i;

        while (y > (j - size)) {
            y--;
            if (y < 0 || matrix[y][i] != '1') {
                return size;
            }
        }

        while (x > (i - size)) {
            x--;
            if (x < 0 || matrix[j][x] != '1') {
                return size;
            }
        }

        return findSquare(matrix, j + 1, i + 1, ++size);
    }

}
