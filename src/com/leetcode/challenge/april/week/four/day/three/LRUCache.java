package com.leetcode.challenge.april.week.four.day.three;

import java.util.*;

class LRUCache {

    public class ListNode {
        int val;
        int key;
        ListNode next;
        ListNode prev;

        ListNode(int key, int val) {
            this.val = val;
            this.key = key;
        }
    }

    private int capacity;
    private ListNode less, most;
    private Map<Integer, ListNode> map;

    public LRUCache(int capacity) {
        this.capacity = capacity;
        map = new HashMap<>();
    }

    public int get(int key) {
        ListNode lNode = map.get(key);
        if (lNode == null) return -1;

        if (lNode != most) {
            if (lNode == less) {
                less = less.next;
            } else {
                lNode.prev.next = lNode.next;
                lNode.next.prev = lNode.prev;
            }
            most.next = lNode;
            lNode.prev = most;
            lNode.next = null;
            most = lNode;
        }

        return lNode.val;
    }

    public void put(int key, int value) {
        ListNode lNode = map.get(key);
        if (lNode == null) {
            lNode = new ListNode(key, value);
            if (map.size() == capacity) {
                map.remove(less.key);
                less = less.next;
            }
            if (less == null) {
                less = lNode;
            } else {
                most.next = lNode;
                lNode.prev = most;
            }
            most = lNode;
            map.put(key, lNode);
        } else {
            lNode.val = value;
            if (lNode != most) {
                if (lNode == less) {
                    less = less.next;
                } else {
                    lNode.prev.next = lNode.next;
                    lNode.next.prev = lNode.prev;
                }
                most.next = lNode;
                lNode.prev = most;
                lNode.next = null;
                most = lNode;
            }
        }
    }
}
