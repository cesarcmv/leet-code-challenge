package com.leetcode.challenge.april.week.four.day.seven;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

public class FirstUnique {

    public static void main(String[] args) {
        FirstUnique firstUnique = new FirstUnique(new int[]{2, 3, 5});
        System.out.println(firstUnique.showFirstUnique());
        firstUnique.add(5);
        System.out.println(firstUnique.showFirstUnique());
        firstUnique.add(2);
        System.out.println(firstUnique.showFirstUnique());
        firstUnique.add(3);
        System.out.println(firstUnique.showFirstUnique());
    }

    private Map<Integer, Integer> map = new HashMap();
    private Queue<Integer> queue = new ArrayDeque();

    public FirstUnique(int[] nums) {
        for (Integer num : nums) {
            queue.add(num);
            addToMap(num);
        }
    }

    public int showFirstUnique() {
        while (!queue.isEmpty() && map.get(queue.peek()) > 1) {
            queue.poll();
        }
        if (queue.isEmpty()) {
            return -1;
        }
        return queue.peek();
    }

    public void add(int value) {
        addToMap(value);
        if (map.get(value) == 1) {
            queue.add(value);
        }
    }

    private void addToMap(Integer key) {
        if (map.containsKey(key)) {
            map.put(key, map.get(key) + 1);
        } else {
            map.put(key, 1);
        }
    }
}
