package com.leetcode.challenge.april.week.four.day.one;

public class SubarraySumEqualsK {

    public static void main(String[] args) {
        int[] nums = new int[]{1, 1, 1};
        SubarraySumEqualsK subarraySumEqualsK = new SubarraySumEqualsK();
        System.out.println(subarraySumEqualsK.subarraySum(nums, 2));
    }

    public int subarraySum(int[] nums, int k) {
        int count = 0;
        for (int j = 0; j < nums.length; j++) {
            int sum = 0;
            for (int i = j; i < nums.length; i++) {
                sum += nums[i];
                if (sum == k) {
                    count++;
                    continue;
                }
            }
        }
        return count;
    }
}
