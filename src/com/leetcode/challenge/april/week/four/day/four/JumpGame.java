package com.leetcode.challenge.april.week.four.day.four;

public class JumpGame {

    public static void main(String[] args) {
        int[] nums = new int[]{1, 2, 0, 1};
        JumpGame jumpGame = new JumpGame();
        System.out.println(jumpGame.canJump(nums));
    }

    public boolean canJump(int[] nums) {
        boolean flag = true;
        int j = 1;
        for (int i = nums.length - 2; i >= 0; i--) {
            if (nums[i] >= j) {
                flag = true;
                j = 1;
            } else {
                flag = false;
                j++;
            }
        }
        return flag;
    }
}
