package com.leetcode.challenge.april.week.three.day.seven;

import java.util.ArrayList;
import java.util.List;

public class LeftmostColumnWithAtLeastAOne {
    public static void main(String[] args) {
        BinaryMatrix binaryMatrix = new BinaryMatrix();
        LeftmostColumnWithAtLeastAOne leftmostColumnWithAtLeastAOne = new LeftmostColumnWithAtLeastAOne();
        System.out.println(leftmostColumnWithAtLeastAOne.leftMostColumnWithOne(binaryMatrix));
    }

    public int leftMostColumnWithOne(BinaryMatrix binaryMatrix) {
        List<Integer> dimensions = binaryMatrix.dimensions();
        int x = dimensions.get(1);
        int y = dimensions.get(0);
        int j = 0;
        int row = -1;
        int i = x - 1;

        while (i > -1) {
            int pos = binaryMatrix.get(j, i);
            if (pos == 1) {
                row = i;
                i--;
                continue;
            } else {
                if (j < y - 1) {
                    j++;
                } else {
                    break;
                }
            }
        }
        return row;
    }
}

class BinaryMatrix {
    int[][] matrix = {
            {0, 0, 0, 1},
            {0, 0, 1, 1},
            {0, 1, 1, 1}
    };

    public int get(int x, int y) {
        return matrix[x][y];
    }

    public List<Integer> dimensions() {
        List<Integer> integers = new ArrayList<>();
        integers.add(matrix.length);
        integers.add(matrix[0].length);
        return integers;
    }
}