package com.leetcode.challenge.april.week.three.day.five;

import java.util.Arrays;

public class SearchInRotatedSortedArray {

    public static void main(String[] args) {

        SearchInRotatedSortedArray searchInRotatedSortedArray = new SearchInRotatedSortedArray();
        int[] nums = new int[]{3, 1};
        System.out.println(searchInRotatedSortedArray.search(nums, 4));
    }

    int n = -1;

    public int search(int[] nums, int target) {
        if (nums.length == 0 || nums.length == 1 && nums[0] != target) {
            return -1;
        }
        if (nums[0] == target) {
            return 0;
        }
        binarySearchDiff(nums, 0, nums.length - 1);
        if (n == -1) {
            n = 0;
        }
        int l = Arrays.binarySearch(nums, 0, n, target);
        int r = Arrays.binarySearch(nums, n, nums.length, target);

        return Math.max(l, r) < 0 ? -1 : Math.max(l, r);
    }

    public void binarySearchDiff(int[] nums, int l, int r) {
        if (r >= l) {
            int mid = l + (r - l) / 2;
            if (mid != 0 && nums[mid] < nums[mid - 1]) {
                n = mid;
                return;
            }
            if (mid > 1) {
                binarySearchDiff(nums, l, mid - 1);
            }
            if (mid <= nums.length - 2) {
                binarySearchDiff(nums, mid + 1, r);
            }
        }
    }
}
