package com.leetcode.challenge.april.week.three.day.one;

public class ProductOfArrayExceptSelf {

    public static void main(String[] args) {
        int[] nums = new int[]{2, 4, 6, 8};
        //[24 ,  12,  8,  6] = 384
        //[192,  96, 64, 48]
        for (Integer num : productExceptSelf(nums)) {
            System.out.println(num);
        }
    }

    public static int[] productExceptSelf(int[] nums) {

        int[] result = new int[nums.length];
        int temp = 1;
        int size = nums.length - 1;
        for (int i = 0; i < nums.length; i++) {
            result[i] = temp * nums[size - i];
            temp = result[i];
        }
        temp = 1;
        for (int i = 0; i < nums.length - 1; i++) {
            result[size - i] = temp * result[size - i - 1];
            temp *= nums[i];
        }
        result[0] = temp;
        for (int i = 0; i < result.length / 2; i++) {
            int aux = result[i];
            result[i] = result[size - i];
            result[size - i] = aux;
        }
        return result;
    }
}
