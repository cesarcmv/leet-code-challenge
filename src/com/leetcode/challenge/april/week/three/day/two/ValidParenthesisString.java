package com.leetcode.challenge.april.week.three.day.two;

import java.util.Stack;

public class ValidParenthesisString {

    public static void main(String[] args) {
        String s = "**((*";
        System.out.println(checkValidString(s));
    }

    public static boolean checkValidString(String s) {
        char[] character = s.toCharArray();
        Stack<Integer> stars = new Stack<>();
        Stack<Integer> open = new Stack<>();
        for (int i = 0; i < character.length; i++) {
            if (character[i] == '(') {
                open.push(i);
            } else if (character[i] == ')') {
                if (open.isEmpty()) {
                    if (stars.isEmpty()) {
                        return false;
                    }
                    stars.pop();
                } else {
                    open.pop();
                }
            } else {
                stars.push(i);
            }
        }
        while (!open.isEmpty()) {
            if (stars.isEmpty() || stars.pop() < open.pop()) {
                return false;
            }
        }
        return true;
    }
}
