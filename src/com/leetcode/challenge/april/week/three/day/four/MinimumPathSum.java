package com.leetcode.challenge.april.week.three.day.four;

public class MinimumPathSum {

    public static void main(String[] args) {
        MinimumPathSum minimumPathSum = new MinimumPathSum();
        int[][] grid = new int[][]{
                {1, 3, 1},
                {1, 5, 1},
                {4, 2, 1}
        };
        System.out.println(minimumPathSum.minPathSum(grid));
    }

    public int minPathSum(int[][] grid) {
        return findPath(grid, 0, 0);
    }

    public int findPath(int[][] grid, int j, int i) {

        if (j == grid.length - 1 && i == grid[j].length - 1) {
            return grid[j][i];
        }

        if (j < grid.length - 1 && i < grid[j].length - 1) {
            int right = grid[j][i] + findPath(grid, j + 1, i);
            int down = grid[j][i] + findPath(grid, j, i + 1);
            return Math.min(right, down);
        }

        if (j < grid.length - 1) {
            return grid[j][i] + findPath(grid, j + 1, i);
        }

        if (i < grid[j].length - 1) {
            return grid[j][i] + findPath(grid, j, i + 1);
        }

        return 0;
    }
}
