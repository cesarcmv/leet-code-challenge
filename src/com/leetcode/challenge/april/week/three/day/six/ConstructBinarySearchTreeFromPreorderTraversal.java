package com.leetcode.challenge.april.week.three.day.six;

import com.leetcode.challenge.TreeNode;

public class ConstructBinarySearchTreeFromPreorderTraversal {

    public static void main(String[] args) {
        ConstructBinarySearchTreeFromPreorderTraversal tree = new ConstructBinarySearchTreeFromPreorderTraversal();
        int[] preorder = new int[]{8, 5, 1, 7, 10, 12};
        tree.bstFromPreorder(preorder);
        tree.printPreorder(tree.root);
    }

    public TreeNode bstFromPreorder(int[] preorder) {

        for (Integer node : preorder) {
            insert(node);
        }

        return root;
    }

    TreeNode root;

    void insert(int key) {
        root = insertRec(root, key);
    }

    TreeNode insertRec(TreeNode root, int key) {

        /* If the tree is empty, return a new node */
        if (root == null) {
            root = new TreeNode(key);
            return root;
        }

        /* Otherwise, recur down the tree */
        if (key < root.val)
            root.left = insertRec(root.left, key);
        else if (key > root.val)
            root.right = insertRec(root.right, key);

        /* return the (unchanged) node pointer */
        return root;
    }

    void printPreorder(TreeNode node)
    {
        if (node == null)
            return;

        /* first print data of node */
        System.out.print(node.val + " ");

        /* then recur on left sutree */
        printPreorder(node.left);

        /* now recur on right subtree */
        printPreorder(node.right);
    }


}
