package com.leetcode.challenge.april.week.three.day.three;

public class NumberOfIslands {

    public static void main(String[] args) {
        NumberOfIslands numberOfIslands = new NumberOfIslands();
        char[][] grid = new char[][]{
                {'1', '1', '0', '0', '0'},
                {'1', '1', '0', '0', '0'},
                {'0', '0', '1', '0', '0'},
                {'0', '0', '0', '1', '1'}
        };
        System.out.println(numberOfIslands.numIslands(grid));
    }

    public int numIslands(char[][] grid) {
        int c = 0;
        for (int j = 0; j < grid.length; j++) {
            for (int i = 0; i < grid[j].length; i++) {
                if (grid[j][i] == '1') {
                    c++;
                    findPath(grid, j, i);
                }
            }
        }
        return c;
    }

    public void findPath(char[][] grid, int j, int i) {
        grid[j][i] = 0;
        if (i - 1 >= 0 && grid[j][i - 1] == '1') {
            findPath(grid, j, i - 1);
        }
        if (i + 1 < grid[j].length && grid[j][i + 1] == '1') {
            findPath(grid, j, i + 1);
        }
        if (j - 1 >= 0 && grid[j - 1][i] == '1') {
            findPath(grid, j - 1, i);
        }
        if (j + 1 < grid.length && grid[j + 1][i] == '1') {
            findPath(grid, j + 1, i);
        }
    }
}
