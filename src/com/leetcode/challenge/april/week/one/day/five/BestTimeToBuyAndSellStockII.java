package com.leetcode.challenge.april.week.one.day.five;

public class BestTimeToBuyAndSellStockII {

    public static void main(String[] args) {
        int[] prices = {2, 1, 2, 0, 1};
        System.out.println(maxProfit(prices));
    }

    public static int maxProfit(int[] prices) {
        int maxProfit = 0;
        int low = 0;
        int max = 0;
        for (int i = 1; i < prices.length; i++) {
            if (max == 0 && prices[i - 1] < prices[i]) {
                low = prices[i - 1];
            }
            if (low >= 0 && prices[i - 1] < prices[i]) {
                max = prices[i];
            }
            if (max > 0 && low >= 0 & prices[i - 1] < prices[i]) {
                maxProfit = maxProfit + max - low;
                low = 0;
                max = 0;
            }
        }
        return maxProfit;
    }
}
