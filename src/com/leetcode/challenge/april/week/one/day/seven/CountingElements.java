package com.leetcode.challenge.april.week.one.day.seven;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CountingElements {

    public static void main(String args){
        int arr[] = new int[]{1,1,3,3,5,5,7,7};
        System.out.println(countElements(arr));
    }

    public static int countElements(int[] arr) {
        List<Integer> list = Arrays.stream(arr).boxed().collect(Collectors.toList());
        Set<Integer> set = list.stream().collect(Collectors.toSet());
        return list
                .stream()
                .filter(i ->
                        set.contains(i + 1))
                .collect(Collectors.toList()).size();
    }
}
