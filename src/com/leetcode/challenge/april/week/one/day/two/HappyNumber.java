package com.leetcode.challenge.april.week.one.day.two;

import java.util.HashSet;
import java.util.Set;

public class HappyNumber {
    public static void main(String[] args) {
        int n = 19;
        System.out.println(isHappy(n));
    }

    public static boolean isHappy(int n) {
        Set<Integer> set = new HashSet<>();
        while (true) {
            int sum = 0;
            while (n > 0) {
                sum = sum + (n % 10) * (n % 10);
                n = n / 10;
            }
            if (!set.add(sum)) {
                return false;
            }
            if (sum == 1) {
                return true;
            }
            n = sum;
        }
    }
}
