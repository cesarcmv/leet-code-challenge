package com.leetcode.challenge.april.week.one.day.four;

import java.util.Arrays;

public class MoveZeros {

    public static void main(String[] args) {
        int[] nums = {0, 1, 0, 3, 12};
        solution(nums);
        System.out.println(Arrays.asList(nums).toString());
    }

    public static void solution(int[] nums) {
        int y = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != 0) {
                nums[y] = nums[i];
                y++;
            }
        }
        while (y < nums.length) {
            nums[y] = 0;
            y++;
        }
    }
}
