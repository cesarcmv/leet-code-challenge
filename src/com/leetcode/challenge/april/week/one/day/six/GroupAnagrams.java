package com.leetcode.challenge.april.week.one.day.six;

import java.util.*;
import java.util.stream.Collectors;

public class GroupAnagrams {

    public static void main(String[] args) {
        String[] strs = {"eat","tea","tan","ate","nat","bat"};
        System.out.println(groupAnagrams(strs));
    }

    public static List<List<String>> groupAnagrams(String[] strs) {
        Map<String, List<String>> groups = new HashMap<>();
        for (int i = 0; i < strs.length; i++) {
            char[] word = strs[i].toCharArray();
            Arrays.sort(word);
            String newWord = String.valueOf(word);
            if (groups.containsKey(newWord)){
                groups.get(newWord).add(strs[i]);
            }else{
                List<String> anagram = new ArrayList<>();
                anagram.add(strs[i]);
                groups.put(newWord, anagram);
            }
        }
        return groups.values().stream().collect(Collectors.toList());
    }
}
