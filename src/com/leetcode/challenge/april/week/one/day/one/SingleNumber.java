package com.leetcode.challenge.april.week.one.day.one;

import java.util.Arrays;

public class SingleNumber {

    public static void main(String[] args) {
        int[] nums = {4,1,2,1,2};
        System.out.println(singleNumber(nums));
    }

    public static int singleNumber(int[] nums) {
        Arrays.sort(nums);
        int i = 1;
        int numsSize = nums.length-1;
        while(i < numsSize){
            if(nums[i-1] == nums[i]){
                if((i+2) > numsSize){
                    return nums[numsSize];
                }
                i += 2;
            }else{
                return nums[i-1];
            }
        }
        return nums[0];
    }
}
