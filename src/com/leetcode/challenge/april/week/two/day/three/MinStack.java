package com.leetcode.challenge.april.week.two.day.three;

public class MinStack {
    int[] stack = null;
    int[] minStack = null;
    int stackSize = 0;
    int minSize = 0;

    public static void main(String[] args) {
        MinStack minStack = new MinStack();
        minStack.push(-2);
        minStack.push(0);
        minStack.push(-3);
        minStack.getMin();
        minStack.pop();
        minStack.top();
        minStack.getMin();
    }

    /**
     * initialize your data structure here.
     */
    public MinStack() {
        stack = new int[]{0, 0, 0, 0, 0};
        minStack = new int[]{0, 0, 0, 0, 0};
    }

    public void push(int x) {
        if (stackSize == stack.length) {
            stack = reSize(stack);
        }
        if (minSize == minStack.length) {
            minStack = reSize(minStack);
        }
        if (minSize == 0 || minStack[minSize - 1] >= x) {
            minStack[++minSize - 1] = x;
        }
        stack[stackSize++] = x;
    }

    public void pop() {
        if (minStack[minSize - 1] == stack[stackSize - 1]) {
            minSize--;
        }
        --stackSize;

    }

    public int top() {
        return stack[stackSize - 1];
    }

    public int getMin() {
        return minStack[minSize - 1];
    }

    public int[] reSize(int[] stack) {
        int[] tStack = new int[stack.length * 2];
        for (int i = 0; i < stack.length; i++) {
            tStack[i] = stack[i];
        }
        stack = tStack;
        return stack;
    }
}
