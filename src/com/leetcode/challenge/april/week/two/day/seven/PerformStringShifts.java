package com.leetcode.challenge.april.week.two.day.seven;

public class PerformStringShifts {

    public static void main(String[] args) {
        String s = "abcdefg";
        int[][] shift = new int[][]{{1, 1}, {1, 1}, {0, 2}, {1, 3}};
        PerformStringShifts performStringShifts = new PerformStringShifts();
        System.out.println(performStringShifts.stringShift(s, shift));
    }

    public String stringShift(String s, int[][] shift) {
        ListNode listNode = createList(s);
        for (int i = 0; i < shift.length; i++) {
            int moves = shift[i][1];
            int count = 0;
            if (shift[i][0] == 0) {
                while (count < moves) {
                    listNode = listNode.next;
                    count++;
                }
            } else {
                while (count < moves) {
                    listNode = listNode.prev;
                    count++;
                }
            }
        }
        return getString(s, listNode);
    }

    public ListNode createList(String s) {
        ListNode temp = new ListNode(s.charAt(0));
        ListNode cNode = temp;
        for (Character c : s.toCharArray()) {
            ListNode nNode = new ListNode(c);
            cNode.next = nNode;
            nNode.prev = cNode;
            cNode = nNode;
        }
        cNode.next = temp.next;
        temp.next.prev = cNode;
        return temp.next;
    }

    public String getString(String s, ListNode listNode){
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            stringBuilder.append(listNode.val);
            listNode = listNode.next;
        }
        return stringBuilder.toString();
    }

    public class ListNode {
        char val;
        ListNode next;
        ListNode prev;

        ListNode(char x) {
            val = x;
        }
    }
}
