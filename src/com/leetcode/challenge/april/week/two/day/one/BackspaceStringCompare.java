package com.leetcode.challenge.april.week.two.day.one;

public class BackspaceStringCompare {

    public static void main(String[] args) {
        String S = "ab#c", T = "ad#c";
        System.out.println(backspaceCompare(S, T));
    }

    public static boolean backspaceCompare(String S, String T) {
        int s = S.length() - 1;
        int t = T.length() - 1;

        int sSkip = 0;
        int tSkip = 0;

        char sChar;
        char tChar;
        while (s > -1 || t > -1) {
            sChar = s > -1 ? S.charAt(s) : 1;
            tChar = t > -1 ? T.charAt(t) : 1;
            if (sChar == '#') {
                sSkip++;
                s--;
                continue;
            }
            if (tChar == '#') {
                tSkip++;
                t--;
                continue;
            }
            if (sSkip > 0) {
                sSkip--;
                s--;
                continue;
            }
            if (tSkip > 0) {
                tSkip--;
                t--;
                continue;
            }

            if (sChar == tChar) {
                s--;
                t--;
            } else {
                return false;
            }
        }

        return true;
    }
}
