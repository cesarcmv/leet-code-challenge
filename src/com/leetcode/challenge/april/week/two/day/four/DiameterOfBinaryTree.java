package com.leetcode.challenge.april.week.two.day.four;

public class DiameterOfBinaryTree {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(4);
        root.left = new TreeNode(-7);
        root.right = new TreeNode(-3);
        root.right.left = new TreeNode(-9);
        root.right.left.left = new TreeNode(9);
        root.right.left.left.left = new TreeNode(6);
        root.right.left.left.left.left = new TreeNode(0);
        root.right.left.left.left.left.right = new TreeNode(-1);
        root.right.left.left.left.right = new TreeNode(6);
        root.right.left.left.left.right.left = new TreeNode(-4);
        root.right.left.right = new TreeNode(-7);
        root.right.left.right.left = new TreeNode(-6);
        root.right.left.right.left.left = new TreeNode(5);
        root.right.left.right.right = new TreeNode(-6);
        root.right.left.right.right.left = new TreeNode(9);
        root.right.left.right.right.left.left = new TreeNode(2);
        root.right.right = new TreeNode(-3);
        root.right.right.left = new TreeNode(-4);

        DiameterOfBinaryTree diameterOfBinaryTree = new DiameterOfBinaryTree();
        System.out.println(diameterOfBinaryTree.diameterOfBinaryTree(root));
    }

    int maxSum = 0;
    public int diameterOfBinaryTree(TreeNode root) {
        if (root == null) return maxSum;

        int lh = height(root.left);
        int rh = height(root.right);
        int sum = lh + rh;

        if (sum > maxSum) maxSum = sum;

        if (lh < rh) {
            return diameterOfBinaryTree(root.right);
        }
        return diameterOfBinaryTree(root.left);
    }

    public int height(TreeNode root) {
        if (root == null) return 0;

        int leftHt = height(root.left);
        int rightHt = height(root.right);

        return Math.max(leftHt, rightHt) + 1;
    }

    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x, TreeNode left, TreeNode right) {
            this.left = left;
            this.right = right;
            this.val = x;
        }

        TreeNode(int x) {
            val = x;
        }
    }
}

