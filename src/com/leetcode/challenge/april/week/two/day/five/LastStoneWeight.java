package com.leetcode.challenge.april.week.two.day.five;

import java.util.*;

public class LastStoneWeight {

    public static void main(String[] args) {
        int[] stones = new int[]{2, 7, 4, 1, 8, 1};
        System.out.println(lastStoneWeight(stones));
    }

    public static int lastStoneWeight(int[] stones) {
        Queue<Integer> priorityQueue = new PriorityQueue<>((Integer o1, Integer o2) -> o2 - o1);
        for (int i = 0; i < stones.length; i++) {
            priorityQueue.add(stones[i]);
        }

        while (true) {
            if (priorityQueue.isEmpty()) {
                return 0;
            }
            if (priorityQueue.size() == 1) {
                return priorityQueue.poll();
            }
            int a = priorityQueue.poll();
            int b = priorityQueue.poll();
            if (a != b) {
                priorityQueue.add(a - b);
            }

        }
    }
}
