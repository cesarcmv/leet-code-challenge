package com.leetcode.challenge.april.week.two.day.two;

import com.leetcode.challenge.ListNode;

public class MiddleOfTheLinkedList {

    public static void main(String[] args) {
        MiddleOfTheLinkedList m = new MiddleOfTheLinkedList();

        ListNode ref1 = new ListNode(1);
        ListNode ref2 = new ListNode(2);
        ListNode ref3 = new ListNode(3);
        ListNode ref4 = new ListNode(4);
        ListNode ref5 = new ListNode(5);
        ListNode ref6 = new ListNode(6);
        ref1.next = ref2;
        ref2.next = ref3;
        ref3.next = ref4;
        ref4.next = ref5;
        ref5.next = ref6;
        ListNode sum = m.middleNode(ref1);
        System.out.println(sum.val);
    }

    public ListNode middleNode(ListNode head) {
        ListNode dRef = head;
        boolean flag = false;
        while (head != null) {
            head = head.next;
            if (flag) {
                dRef = dRef.next;
            }
            flag = !flag;
        }
        return dRef;
    }
}
