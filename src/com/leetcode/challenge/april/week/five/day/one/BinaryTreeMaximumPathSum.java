package com.leetcode.challenge.april.week.five.day.one;

import com.leetcode.challenge.TreeNode;

public class BinaryTreeMaximumPathSum {

    public static void main(String[] args) {

        BinaryTreeMaximumPathSum pathSum = new BinaryTreeMaximumPathSum();
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(-2);
        root.right = new TreeNode(3);
        //root.right.left = new TreeNode(15);
        //root.right.right = new TreeNode(7);
        System.out.println(pathSum.maxPathSum(root));
    }

    int ans;

    public int maxPathSum(TreeNode root) {
        ans = root.val;
        depth(root);
        return ans;
    }

    public int depth(TreeNode node) {
        if (node == null) return 0;
        int L = depth(node.left);
        int R = depth(node.right);
        ans = Math.max(L + R + node.val, ans);
        int result = Math.max(L, R) + node.val;
        return result < 0 ? 0 : result;
    }

}
