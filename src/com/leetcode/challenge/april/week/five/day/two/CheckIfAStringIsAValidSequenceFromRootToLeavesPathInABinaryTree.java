package com.leetcode.challenge.april.week.five.day.two;

import com.leetcode.challenge.TreeNode;

public class CheckIfAStringIsAValidSequenceFromRootToLeavesPathInABinaryTree {

    public boolean isValidSequence(TreeNode root, int[] arr) {
        int n = arr.length;
        int pos = 0;
        return isValidSequence(root, n, pos, arr);
    }

    public boolean isValidSequence(TreeNode root, int n, int pos, int[] arr) {
        if (root == null) {
            return false;
        } else if (pos == n)
            return false;
        else if (root.val != arr[pos])
            return false;
        else if (root.left != null && root.right != null && pos == n - 1)
            return true;

        return (isValidSequence(root.left, n, pos + 1, arr) || isValidSequence(root.right, n, pos + 1, arr));
    }

}
