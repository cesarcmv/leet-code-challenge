package com.leetcode.challenge.may.week.two.day.seven;

public class TrieNode {
    TrieNode[] children = new TrieNode[26];
    boolean end;

    TrieNode(){
        end = false;
        for (int i = 0; i < 26; i++)
            children[i] = null;
    }
}
