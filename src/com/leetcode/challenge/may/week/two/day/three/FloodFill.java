package com.leetcode.challenge.may.week.two.day.three;

public class FloodFill {

    public static void main(String[] args) {
        FloodFill floodFill = new FloodFill();
        int[][] grid = new int[][]{
                {0, 0, 0},
                {0, 1, 1}
        };
        System.out.println(floodFill.floodFill(grid, 1, 1, 1));
    }

    public int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
        int oldColor = image[sr][sc];
        fill(image, sr, sc, oldColor, newColor);
        return image;
    }

    public void fill(int[][] grid, int j, int i, int oldColor, int newColor) {
        grid[j][i] = newColor;
        if (i - 1 >= 0 && grid[j][i - 1] == oldColor) {
            fill(grid, j, i - 1, oldColor, newColor);
        }
        if (i + 1 < grid[j].length && grid[j][i + 1] == oldColor) {
            fill(grid, j, i + 1, oldColor, newColor);
        }
        if (j - 1 >= 0 && grid[j - 1][i] == oldColor) {
            fill(grid, j - 1, i, oldColor, newColor);
        }
        if (j + 1 < grid.length && grid[j + 1][i] == oldColor) {
            fill(grid, j + 1, i, oldColor, newColor);
        }
    }
}
