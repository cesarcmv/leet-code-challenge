package com.leetcode.challenge.may.week.two.day.one;

public class ValidPerfectSquare {

    public static void main(String[] args) {
        ValidPerfectSquare square = new ValidPerfectSquare();
        System.out.println(square.isPerfectSquare(2147483647));
    }

    public boolean isPerfectSquare(int num) {

        long l = 1;
        long h = 1000000;

        while (l <= h) {
            long mid = l + (h - l) / 2;
            long mid2 = mid * mid;
            if (mid2 == num) {
                return true;
            } else if (num < mid2) {
                h = mid - 1;
            } else if (num > mid2) {
                l = mid + 1;
            }
        }
        return false;
    }
}
