package com.leetcode.challenge.may.week.two.day.six;

import java.util.Stack;

public class RemoveKDigits {

    public static void main(String[] args) {
        RemoveKDigits removeKDigits = new RemoveKDigits();
        System.out.println(removeKDigits.removeKdigits("10200", 1));
    }

    public String removeKdigits(String num, int k) {

        char[] nums = num.toCharArray();
        Stack<Character> mystack = new Stack<>();
        //Store the final string in stack
        for (Character c : nums) {
            while (!mystack.empty() && k > 0 && mystack.peek() > c) {
                mystack.pop();
                k -= 1;
            }

            if (!mystack.empty() || c != '0')
                mystack.push(c);
        }

        //Now remove the largest values from the top of the stack
        while (!mystack.isEmpty() && k != 0) {
            mystack.pop();
            k--;
        }
        if (mystack.empty())
            return "0";

        Stack<Character> temp = new Stack<>();
        while (!mystack.isEmpty()) {
            temp.push(mystack.pop());
        }
        StringBuilder sb = new StringBuilder();
        while (!temp.isEmpty()) {
            sb.append(temp.pop());
        }

        return sb.toString();
    }
}
