package com.leetcode.challenge.may.week.two.day.seven;

// Java implementation of search and insert operations
// on Trie
public class Trie2 {
    TrieNode root = null;

    public Trie2() {
        root = new TrieNode();
    }

    void insert(String key) {
        int index;
        TrieNode pNode = root;
        for (int i = 0; i < key.length(); i++) {
            index = key.charAt(i) - 'a';
            if (pNode.children[index] == null) {
                pNode.children[index] = new TrieNode();
            }
            pNode = pNode.children[index];
        }
        pNode.end = true;
        
    }

    boolean search(String key) {
        TrieNode pNode = root;
        for (int i = 0; i < key.length(); i++) {
            int index = key.charAt(i) - 'a';
            if (pNode.children[index] == null) {
                return false;
            }
            pNode = pNode.children[index];
        }
        return (pNode != null && pNode.end);
    }

    boolean startsWith(String key) {
        TrieNode pNode = root;
        for (int i = 0; i < key.length(); i++) {
            int index = key.charAt(i) - 'a';
            if (pNode.children[index] == null) {
                return false;
            }
            pNode = pNode.children[index];
        }
        return true;
    }

    public static void main(String[] args) {
        Trie2 trie = new Trie2();

        trie.insert("apple");
        trie.search("apple");   // returns true
        trie.search("app");     // returns false
        trie.startsWith("app"); // returns true
        trie.insert("app");
        trie.search("app");     // returns true
    }
}
