package com.leetcode.challenge.may.week.one.day.two;

import java.util.HashSet;
import java.util.Set;

public class JewelsAndStones {

    public int numJewelsInStones(String J, String S) {
        Set<Character> set = new HashSet<>();
        int j = 0;
        for (Character c : J.toCharArray()) {
            set.add(c);
        }
        for (Character c : S.toCharArray()) {
            if(set.contains(c)){
                j++;
            }
        }
        return j;
    }
}
