package com.leetcode.challenge.may.week.one.day.three;

import javax.security.sasl.SaslServer;
import java.util.HashMap;
import java.util.Map;

public class RansomNote {

    public static void main(String[] args){
        RansomNote ransomNote = new RansomNote();
        System.out.println(ransomNote.canConstruct("aa", "ab"));

    }

    public boolean canConstruct(String ransomNote, String magazine) {
        Map<Character, Integer> maga = new HashMap<>();

        for (Character c : magazine.toCharArray()) {
            if (maga.containsKey(c)) {
                maga.put(c, maga.get(c) + 1);
            } else {
                maga.put(c, 1);
            }
        }

        for (Character c : ransomNote.toCharArray()) {
            if (maga.containsKey(c)) {
                int count = maga.get(c) - 1;
                maga.put(c, count);
                if(count == 0){
                    maga.remove(c);
                }
;            } else {
                return false;
            }
        }
        return true;
    }
}
