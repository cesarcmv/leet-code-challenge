package com.leetcode.challenge.may.week.one.day.seven;

import com.leetcode.challenge.TreeNode;

public class CousinsInBinaryTree {

    TreeNode parentNode = null;

    public boolean isCousins(TreeNode root, int x, int y) {

        int xLevel = dfs(root, null, x, 0);
        TreeNode xPNode = parentNode;

        int yLevel = dfs(root, null, y, 0);
        TreeNode yPNode = parentNode;

        return xLevel == yLevel && xPNode != yPNode;
    }

    int dfs(TreeNode root, TreeNode pNode, int n, int level){

        if(root == null){
            return -1;
        }

        if(root.val == n){
            parentNode = pNode;
            return level;
        }

        level++;

        int l = dfs(root.left, root, n, level);
        if(l != -1){
            return l;
        }

        int r = dfs(root.right, root, n, level);
        if( r != -1){
            return r;
        }

        return -1;
    }
}
