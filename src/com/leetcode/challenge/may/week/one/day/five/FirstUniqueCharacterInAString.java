package com.leetcode.challenge.may.week.one.day.five;

public class FirstUniqueCharacterInAString {

    public static void main(String[] args) {
        FirstUniqueCharacterInAString string = new FirstUniqueCharacterInAString();
        System.out.println(string.firstUniqChar("leetcode"));
    }

    public int firstUniqChar(String s) {
        int[] count = new int[26];

        for (int i = 0; i < s.length(); i++) {
            count['z' - s.charAt(i)]++;
        }

        for (int i = 0; i < s.length(); i++) {
            if (count['z' - s.charAt(i)] == 1) {
                return i;
            }
        }

        return -1;
    }
}
