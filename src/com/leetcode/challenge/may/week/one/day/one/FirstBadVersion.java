package com.leetcode.challenge.may.week.one.day.one;

abstract class VersionControl {
    boolean isBadVersion(int index) {
        return true;
    }
}

public class FirstBadVersion extends VersionControl {
    public int firstBadVersion(int n) {
        int start = 1;
        int end = n;
        while (start < end) {
            int index = start + (end - start) / 2;
            if (isBadVersion(index)) {
                end = index;
            } else {
                start = index + 1;
            }
        }
        return start;
    }
}
