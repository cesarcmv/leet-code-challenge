package com.leetcode.challenge.may.week.one.day.four;

public class NumberComplement {

    public static void main(String args[]) {
        NumberComplement complement = new NumberComplement();
        System.out.println(complement.findComplement(1));
    }

    public int findComplement(int num) {
        String sb = Integer.toBinaryString(num);
        int r = 0;
        int nSize = sb.length() - 1;
        for (int i = 0; i < nSize; i++) {
            if (sb.charAt(nSize - i) == '0') {
                r += Math.pow(2, i);
            }
        }
        return r;
    }
}
