package com.leetcode.challenge.may.week.three.three;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FindAllAnagramsInAString {

    public static void main(String[] args) {
        FindAllAnagramsInAString anagrams = new FindAllAnagramsInAString();
        System.out.println(anagrams.findAnagrams("", "a"));
    }

    public List<Integer> findAnagrams(String s, String p) {
        if(p.length() > s.length()){
            return new ArrayList<>();
        }
        char[] sArray = new char[26];
        char[] pArray = new char[26];
        int i = 0;
        List<Integer> list = new ArrayList();

        for (i = 0; i < p.length(); i++) {
            pArray['z' - p.charAt(i)]++;
            sArray['z' - s.charAt(i)]++;
        }

        if (Arrays.equals(sArray, pArray)) {
            list.add(0);
        }

        while (i < s.length()) {
            sArray['z' - s.charAt(i - p.length())]--;
            sArray['z' - s.charAt(i)]++;
            if (Arrays.equals(sArray, pArray)) {
                list.add(i - p.length() + 1);
            }
            i++;
        }

        return list;
    }
}
