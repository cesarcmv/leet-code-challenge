package com.leetcode.challenge.may.week.three.six;

import com.leetcode.challenge.TreeNode;

import java.util.Collection;
import java.util.Collections;
import java.util.PriorityQueue;

public class KthSmallestElementInABST {

    public static void main(String[] arg) {
        TreeNode node = new TreeNode(3);
        node.left = new TreeNode(1);
        node.left.right = new TreeNode(2);
        node.right = new TreeNode(4);

        KthSmallestElementInABST kth = new KthSmallestElementInABST();
        System.out.println(kth.kthSmallest(node, 1));
    }

    public int kthSmallest(TreeNode root, int k) {
        PriorityQueue<Integer> pQueue = new PriorityQueue<Integer>(Collections.reverseOrder());
        inOrder(root, pQueue);
        int c = 1;
        while(!pQueue.isEmpty()){
            int number = pQueue.poll();
            if(c == k){
                return number;
            }
            c++;
        }
        return -1;
    }

    public void inOrder(TreeNode root, PriorityQueue<Integer> pQueue){
        if (root == null) {
            return;
        }
        inOrder(root.left, pQueue);
        pQueue.add(root.val);
        inOrder(root.right, pQueue);
    }
}
