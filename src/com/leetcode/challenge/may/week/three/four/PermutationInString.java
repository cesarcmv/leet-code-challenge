package com.leetcode.challenge.may.week.three.four;

import java.util.ArrayList;
import java.util.Arrays;

public class PermutationInString {

    public static void main(String[] args) {
        PermutationInString anagrams = new PermutationInString();
        System.out.println(anagrams.checkInclusion("a", "ab"));
    }

    public boolean checkInclusion(String s1, String s2) {
        if(s1.length() > s2.length()){
            return false;
        }
        char[] sArray = new char[26];
        char[] pArray = new char[26];
        int i = 0;

        for (i = 0; i < s1.length(); i++) {
            pArray['z' - s1.charAt(i)]++;
            sArray['z' - s2.charAt(i)]++;
        }

        while (i < s2.length()) {
            if (Arrays.equals(sArray, pArray)) {
                return true;
            }
            sArray['z' - s2.charAt(i - s1.length())]--;
            sArray['z' - s2.charAt(i)]++;
            i++;
        }
        return false;
    }
}
