package com.leetcode.challenge.may.week.three.one;

public class MaximumSumCircularSubarray {

    public static void main(String[] args) {
        MaximumSumCircularSubarray m = new MaximumSumCircularSubarray();
        int[] c = new int[]{5, -3, -2, 6, -1, 4};
        m.maxSubarraySumCircular(c);
    }

    public int maxSubarraySumCircular(int[] A) {
        int maxSum = Integer.MIN_VALUE;
        int minSum = Integer.MAX_VALUE;
        int cMaxSum = 0;
        int cMinSum = 0;
        int totalSum = 0;
        for (int i = 0; i < A.length; i++) {
            totalSum += A[i];
            cMaxSum += A[i];
            cMinSum += A[i];
            if (cMaxSum > maxSum) {
                maxSum = cMaxSum;
            }
            if (cMaxSum < 0) {
                cMaxSum = 0;
            }

            if (cMinSum < minSum) {
                minSum = cMinSum;
            }
            if (cMinSum > 0) {
                cMinSum = 0;
            }
        }

        return totalSum == minSum ? maxSum : Math.max(maxSum, (totalSum - minSum));
    }
}
