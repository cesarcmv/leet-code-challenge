package com.leetcode.challenge.may.week.three.five;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class StockSpanner {

    List<Integer> list = new ArrayList<>();

    public static void main(String[] args) {
        StockSpanner S = new StockSpanner();
        System.out.println(S.next(28));//1
        System.out.println(S.next(14));//2
        System.out.println(S.next(28));//1
        System.out.println(S.next(35));//2
        System.out.println(S.next(46));//1
        System.out.println(S.next(53));//1
        System.out.println(S.next(66));//1
        System.out.println(S.next(80));//1
        System.out.println(S.next(87));//1
        System.out.println(S.next(88));//1
    }

    public StockSpanner() {

    }

    public int next(int price) {
        int c = 0;
        int min = Integer.MAX_VALUE;
        for (int i = list.size() - 1; i > -1; i--) {
            if (price > list.get(i)) {
                c++;
            } else {
                break;
            }
        }
        list.add(price);

        return c + 1;
    }
}
