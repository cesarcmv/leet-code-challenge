package com.leetcode.challenge.may.week.three.two;

import com.leetcode.challenge.ListNode;

public class OddEvenLinkedList {

    public static void main(String[] args) {
        OddEvenLinkedList oddEvenLinkedList = new OddEvenLinkedList();

        ListNode node = new ListNode(1);
        node.next = new ListNode(2);
        node.next.next = new ListNode(3);
        node.next.next.next = new ListNode(4);
        node.next.next.next.next = new ListNode(5);
        node.next.next.next.next.next = new ListNode(6);
        node.next.next.next.next.next.next = new ListNode(7);
        node.next.next.next.next.next.next.next = new ListNode(8);

        oddEvenLinkedList.oddEvenList(node);
    }

    public ListNode oddEvenList(ListNode head) {

        if (head == null) {
            return head;
        }

        ListNode odd = head;
        ListNode pair = head.next;
        ListNode pairH = head.next;
        boolean flag = true;
        while (true) {
            if (odd == null || pair == null ) {
                break;
            }
            if (flag) {
                odd.next = pair.next;
                if(odd.next == null){
                    break;
                }
                odd = odd.next;
                flag = false;
            } else {
                pair.next = odd.next;
                if(pair.next == null){
                    break;
                }
                pair = pair.next;
                flag = true;
            }
        }

        odd.next = pairH;

        return head;
    }
}
